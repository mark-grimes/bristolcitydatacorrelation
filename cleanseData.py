# coding=utf-8

import json, datetime

# Get this data from https://opendata.bristol.gov.uk/explore/dataset/deprivation-in-bristol-2019/download/?format=json&timezone=Europe/London
with open('deprivation-in-bristol-2019.json') as inputFile:
    input=json.load(inputFile)
print "JSON input loaded, there are", len(input), "records"


# Get this data as an excel sheet from https://drive.google.com/drive/folders/1-G_T-ZcH0PTuILT2VybBKOxKwwCdd4Ja, download "ward data.xlsx"
# and then convert to csv file.
beginReading=False
wardData={}
with open('self-harm.csv') as inputFile:
    for line in inputFile:
        values=line.split(',')
        if values[0]=="Ward" : beginReading=True
        if beginReading :
            wardData[values[0]]=values[1:-1]

# Loop over all records in the deprivation and try to match to the self harm data
for record in input:
    wardName=record["fields"]["2016_ward_name_based_on_pwc"]
    print wardName
